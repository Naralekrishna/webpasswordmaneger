import './App.css';
import './../node_modules/bootstrap/dist/css/bootstrap.css';
import Home from './components/pages/Home';
import Add from './components/webSites/Add';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import NotFound from './components/pages/NotFound';
import Edit from './components/webSites/Edit';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/add" element={<Add />} />
          <Route path="/edit/:id" element={<Edit />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
