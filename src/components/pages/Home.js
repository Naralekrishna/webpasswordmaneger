import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';
import configObject from '../../configs/configObject';


const Home = () => {
    const [webSites, setWebSite] = useState([]);
    const [checkedState, setCheckedState] = useState([]);
    let navigate = useNavigate();

    const handleOnChange = (e, id) => {
        if (e.target.checked === true) {
            setCheckedState([...checkedState, id]);
        } else {
            const index = checkedState.indexOf(id);
            let tempArray = [...checkedState];
            tempArray.splice(index, 1);
            setCheckedState([...tempArray]);
        }
    };

    const loadWebSites = async () => {
        try {
            const webSitesList = await axios.get(`${configObject.API_BASE_URL}/webSites`);
            setWebSite(webSitesList.data.data);
        } catch (error) {
            if (error.response) { alert(error.response.data.message) } else { alert(error.message); };
        }
    }

    useEffect(() => {
        loadWebSites();
    }, []);

    const handleSubmit = async (event) => {
        try {
            event.preventDefault();
            if (event.nativeEvent.submitter.id === `edit`) {
                if (checkedState.length === 0) { alert(`please select row first`); return; };
                navigate(`/edit/${checkedState[0]}`);
            } else {
                if (checkedState.length === 0) { alert(`please select row first`); return; };
                const webSiteDelete = await axios.post(`${configObject.API_BASE_URL}/webSites/delete`, { ids: checkedState });
                if (webSiteDelete.data.status === 200) { loadWebSites(); setCheckedState([]); alert(webSiteDelete.data.message) }
            }
        } catch (error) {
            if (error.response) { alert(error.response.data.message) } else { alert(error.message); };
        }
    }

    const listRows = () => {
        if (webSites.length > 0) {
            return webSites.map((site, index) => {
                return (
                    <tr key={site.id}>
                        <td><input type="checkbox" name={site.web_site} value={site.web_site} onChange={(e) => handleOnChange(e, site.id)} /></td>
                        <td>{site.web_site}</td>
                        <td>{site.password}</td>
                    </tr>
                )
            })
        } else {
            return (
                <tr key="0">
                    <td colSpan="3" className="text-center"><strong>No Records</strong></td>
                </tr>
            )
        }
    }


    return (
        <div className="home container">
            <form onSubmit={handleSubmit}>
                <h1 className="text-center">Web Based Password Manager</h1>
                <div className="py-3">
                    <button className="d-inline text-primary btn btn-link mr-2" disabled={checkedState.length > 1 || checkedState.length === 0} type="submit" id="edit"><u>Edit</u></button>
                    <button className="d-inline text-primary btn btn-link" type="submit" id="delete" disabled={checkedState.length === 0}><u>Delete</u></button>
                    <Link to="/add" className="float-end">Generate New Password</Link>
                </div>
                <table className="table table-striped table-hover table-bordered">
                    <thead className="shadow">
                        <tr>
                            <th scope="col"><input type="checkbox" disabled /></th>
                            <th scope="col">Web Site Name</th>
                            <th scope="col">Password</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            listRows()
                        }
                    </tbody>
                </table>
                <Link to="/add" className="d-flex justify-content-end">Generate New Password</Link>
            </form>
        </div>
    )
}

export default Home;