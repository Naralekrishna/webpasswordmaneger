import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import passwordGenerator from 'generate-password';
import configObject from '../../configs/configObject';

const Add = () => {
    const [webSite, setWebSite] = useState({ web_site: "", password: "" });
    let { web_site, password } = webSite;
    let navigate = useNavigate();

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setWebSite(values => ({ ...values, [name]: value }));
    }

    const handleSubmit = async (event) => {
        try {
            event.preventDefault();
            if (webSite.password === "") { alert(`please generate password to save`); return; };
            const webSiteInserted = await axios.post(`${configObject.API_BASE_URL}/webSites`, webSite);
            if (webSiteInserted.data.status === 201) { navigate(`/`); alert(webSiteInserted.data.message) }
        } catch (error) {
            if (error.response) { alert(error.response.data.message) } else { alert(error.message); };
        }
    }

    const generatePassword = () => {
        let generatedPassword = passwordGenerator.generate({
            length: 10,
            numbers: true
        });
        setWebSite({ ...webSite, password: generatedPassword });
    }

    const backToHome = () => {
        navigate(`/`);
    }

    return (
        <div className="container">
            <div className="row mt-4">
                <div className="card mt-4 col-md-6 offset-md-3 bg-secondary">
                    <div className="card-body">
                        <h5 className="card-title text-center">Generate New Password</h5>
                        <form onSubmit={handleSubmit}>
                            <div className="row">
                                <div className="col-md-8">
                                    <label className="form-label text-white">Enter Web Site Name</label>
                                    <input type="text" className="form-control" name="web_site" value={web_site} onChange={handleChange} required />
                                </div>
                                <div className="col-md-4">
                                    <button type="button" className="btn btn-info my-4" onClick={generatePassword}>Generate Password</button>
                                </div>
                            </div>
                            <div className="row col-md-12 mb-2">
                                <div>
                                    <input type="text" className="form-control" id="" placeholder="Generated password will be shown here" name="password" value={password} onChange={handleChange} disabled />
                                </div>
                            </div>
                            <button type="submit" className="btn btn-primary">Save</button>
                            <button type="button" className="btn btn-primary mx-2" onClick={backToHome}>Cancel</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Add;